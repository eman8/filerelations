# FileRelations

Ce plugin propose les mêmes fonctionnalités que notre version du plugin [ItemRelations](https://gitlab.com/eman8/ItemRelations), mais pour les fichiers.

## Credits

Plugin réalisé pour la plate-forme EMAN (ENS-CNRS-Sorbonne nouvelle) par Vincent Buard (Numerizen) avec le soutien de l’IRIHS – Université de Rouen Normandie : https://irihs.univ-rouen.fr. Voir les explications sur le site [EMAN](https://eman-archives.org/EMAN/pluginseman#B7).
