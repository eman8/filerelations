<div id="file-relations-display-file-relations">
    <?php if (!$subjectRelations && !$objectRelations): ?>
    <p><?php echo "Ce fichier n'a pas de relation indiqu�e avec un autre fichier du projet."; ?></p>
    <?php else: ?>
    <table>
    		<?php $previous_relation = ""; ?>
    		<?php foreach ($subjectRelations as $subjectRelation): ?>
	        <?php if ($previous_relation <> $subjectRelation['relation_text']) { ?>
		        <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>  		        
                <?php $intitule = get_option('fr_intitule', 'Ce fichier');	?>      
		        		<td><?php echo __($intitule);?>
	            	<span title="<?php echo html_escape($subjectRelation['relation_description']); ?>" style="font-style:italic;"><?php echo strip_tags($subjectRelation['relation_text']); ?> : </span>
	            	</td>
		     		</tr>
          <?php } ?>
		     	
		     	<tr>
              <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	            <td>
	            	<?php // echo $subjectRelation['collection_thumbnail']; ?>
              	<a style='line-height:40px;margin-left:5px;' href="<?php echo url('files/show/' . $subjectRelation['object_file_id']); ?>"><?php echo $subjectRelation['object_file_title']; ?></a>
	            	<br /><div class='relation-comment' style='clear:both;background:#ddd;font-style:italic;'><?php echo $subjectRelation['relation_comment']; ?></div>	            	
	            </td>
	            </tr>
		     	<?php
					  $previous_relation = $subjectRelation['relation_text'];          
          endforeach; ?>
          <tr><td colspan=2><hr /></td></tr>
    		<?php $previous_relation = ""; ?>
    		<?php foreach ($objectRelations as $objectRelation): ?>
	        <?php if ($previous_relation <> $objectRelation['relation_text']) { ?>
		     	<?php $previous_relation = $objectRelation['relation_text'];} ?>        
	        <tr>
              <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>  
	            <td><a style='line-height:40px;margin-left:5px;' href="<?php echo url('files/show/' . $objectRelation['subject_file_id']); ?>"><?php echo $objectRelation['subject_file_title']; ?></a>		            
	            </td>	            		            	
	            </tr>
		        <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>  		        
                <?php $intitule = get_option('fr_intitule_obj');
                  $intitule ? null : $intitule = 'ce fichier';	?>                       		        
		        		<td><span title="<?php echo html_escape($objectRelation['relation_description']); ?>" style="font-style:italic;"><?php echo $objectRelation['relation_text']; echo '  ' . $intitule; ?> </span>
<div class='relation-comment' style='clear:both;background:#ddd;font-style:italic;'><?php echo $objectRelation['relation_comment']; ?></div>		        		
	            	</td>
		     		</tr>	            
		     	<?php $previous_relation = ""; ?>	        
        <?php endforeach; ?>
    </table>
    <?php endif; ?>
</div>
